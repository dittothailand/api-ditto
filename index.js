require('dotenv').config()

const port = process.env.PORT
const base = process.env.API_BASE
const csvFilePath = process.env.CSV_PATH_RELATIVE

const {csv} =require('csvtojson')
const _ = require('underscore');

const fastify = require('fastify')({ logger: true });

// fastify.register(require('fastify-formbody'))
// fastify.register(require('fastify-multipart'));

/* ------------------------------------ GET ----------------------------------- */

fastify.get(`${base}/`, (req, reply) => { // get status
    try {
    reply.send({'msg':'Ditto api online'})
    } catch(error) {
    reply.send({'err':error})
    }
})

fastify.get(`${base}/getcsv/`, async (req, reply) => { // get status
  try {
   // get files
  let reports = await csv().fromFile(csvFilePath); // [{}]
  reply.send(reports)
  } catch(error) {
  reply.send({'err':error})
  }
})

// Run the server!
fastify.listen(port, function (err, address) {
  if (err) {
    fastify.log.error(err)
    process.exit(1)
  }
  fastify.log.info(`server listening on ${address}`)
})

